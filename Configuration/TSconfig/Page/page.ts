## Configuration for copy & paste (production)
TCEMAIN.table.pages {
    disablePrependAtCopy = 1
    disableHideAtCopy = 0
}
TCEMAIN.table.tt_content {
    disablePrependAtCopy = 1
    disableHideAtCopy = 0
}

## Configuration for copy & paste (deployment)
[applicationContext == "Development"]
    TCEMAIN.table.pages {
        disablePrependAtCopy = 1
        disableHideAtCopy = 1
    }
    TCEMAIN.table.tt_content {
        disablePrependAtCopy = 1
        disableHideAtCopy = 1
    }
[END]

## Content configuration
TCEFORM.tt_content {
    header_layout {
        altLabels {
            0 = Standardüberschrift [h2]
            1 = Überschrift 1. Ordnung [h1]
            2 = Überschrift 2. Ordnung [h2]
            3 = Überschrift 3. Ordnung [h3]
            4 = Überschrift 4. Ordnung [h4]
            5 = Überschrift 5. Ordnung [h5]
        }
        addItems  {
            6 = Überschrift 6. Ordnung [h6]
        }
        removeItems = 100
    }
    frame_class {
        addItems {
            graybox = graue Box
            bluebox = blaue Box
        }
        #removeItems = default,none
        removeItems = ruler-before,ruler-after,indent,indent-left,indent-right,graybox,bluebox
    }
    layout {
        addItems {
            4 = Layout 0815
        }
        removeItems = 1,2,3,4
    }
}

## [17.04.2019] OM: Moved to kitt3n_custom
## Frontend layouts
#TCEFORM.pages.layout {
#    altLabels {
#        1 = Layout 1
#        2 = Layout 2
#        3 = Layout 3
#    }
#    addItems {
#        4 = Layout 4
#    }
#    removeItems = 1,2,3,4
#}

## Custom image cropping
TCEFORM.sys_file_reference.crop.config.cropVariants {
    default {
        title = Desktop
        selectedRatio = NaN
        allowedAspectRatios {
            NaN {
                title = Frei
                value = 0.0
            }
            2:3 {
                title = 2:3
                value = 0.6666666
            }
            3:4 {
                title = 3:4
                value = 0.75
            }
            1:1 {
                title = 1:1
                value = 1.0
            }
            4:3 {
                title = 4:3
                value = 1.3333333333333333
            }
            3:2 {
                title = 3:2
                value = 1.5
            }
            2:1 {
                title = 2:1
                value = 2.0
            }
            16:9 {
                title = 16:9
                value = 1.777777777777777
            }
            21:9 {
                title = 21:9
                value = 2.3333333333333333
            }
        }
        focusArea {
            x = 0.33333333333,
            y = 0.33333333333,
            width = 0.33333333333,
            height = 0.33333333333,
        }
    }
    tablet {
        title = Tablet
        selectedRatio = NaN
        allowedAspectRatios {
            NaN {
                title = Frei
                value = 0.0
            }
            2:3 {
                title = 2:3
                value = 0.6666666
            }
            3:4 {
                title = 3:4
                value = 0.75
            }
            1:1 {
                title = 1:1
                value = 1.0
            }
            4:3 {
                title = 4:3
                value = 1.3333333333333333
            }
            3:2 {
                title = 3:2
                value = 1.5
            }
            2:1 {
                title = 2:1
                value = 2.0
            }
            16:9 {
                title = 16:9
                value = 1.777777777777777
            }
            21:9 {
                title = 21:9
                value = 2.3333333333333333
            }
        }
        focusArea {
            x = 0.33333333333,
            y = 0.33333333333,
            width = 0.33333333333,
            height = 0.33333333333,
        }
    }
    mobile {
        title = Smartphone
        selectedRatio = NaN
        allowedAspectRatios {
            NaN {
                title = Frei
                value = 0.0
            }
            2:3 {
                title = 2:3
                value = 0.6666666
            }
            3:4 {
                title = 3:4
                value = 0.75
            }
            1:1 {
                title = 1:1
                value = 1.0
            }
            4:3 {
                title = 4:3
                value = 1.3333333333333333
            }
            3:2 {
                title = 3:2
                value = 1.5
            }
            2:1 {
                title = 2:1
                value = 2.0
            }
            16:9 {
                title = 16:9
                value = 1.777777777777777
            }
            21:9 {
                title = 21:9
                value = 2.3333333333333333
            }
        }
        focusArea {
            x = 0.33333333333,
            y = 0.33333333333,
            width = 0.33333333333,
            height = 0.33333333333,
        }
    }
}

## RTE: Configuration CKEditor (presets: default, minimal, full)
RTE.default.preset = custom

## Default backend language
mod.SHARED {
    defaultLanguageFlag = de
    defaultLanguageLabel = Deutsch
}

## Default backend group for new pages
# TCEMAIN {
#     permissions {
#         groupid = 2
#         group = show,editcontent,edit,new,delete
#     }
# }