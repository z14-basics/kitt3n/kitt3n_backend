<?php
defined('TYPO3_MODE') || die('Access denied.');

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$boot = function () {

    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_backend')) {

        ## Include custom RTE settings
        $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['custom'] = 'EXT:kitt3n_backend/Configuration/RTE/custom.yaml';

        ## Add PAGE TSConfig
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_backend/Configuration/TSconfig/Page" extensions="ts,txt">');

        ## Add USER TSConfig
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_backend/Configuration/TSconfig/User" extensions="ts,txt">');
    }

};

$boot();
unset($boot);